"""Recurso dos Relacionamentos."""
from flask import current_app
from flask_restful import Resource
from flask_restful.reqparse import RequestParser
from flask_jwt_extended import jwt_required
from ..models import Relacionamentos, RelacionamentosSchema, Topicos
from marshmallow.exceptions import ValidationError
from . import verificaNivel


class RelacionamentoBase(Resource):
    """Classe Base dos Recursos."""

    parser = RequestParser()
    parser.add_argument("descricao")
    parser.add_argument("topicos1_id")
    parser.add_argument("topicos2_id")


class RelacionamentoTodosAceitos(RelacionamentoBase):
    """Classe para Mostrar Todos Relacionamentos aceitos."""

    @jwt_required
    def get(self):
        """Retorna todos os relacionamentos."""
        return {
            "relacionamentos": RelacionamentosSchema(many=True).dump(
                Relacionamentos.query.filter(Relacionamentos.status == 1).all()
            )
        }


class RelacionamentoTodosDesativados(RelacionamentoBase):
    """Classe para Mostrar Todos Relacionamentos esperando aceitar ou rejeitado."""

    @jwt_required
    def get(self):
        """Retorna todos os relacionamentos."""
        return {
            "relacionamentos": RelacionamentosSchema(many=True).dump(
                Relacionamentos.query.filter(Relacionamentos.status != 1).all()
            )
        }


class Relacionamento(RelacionamentoBase):
    """Classe Para Executar CRUD dos Relacionamentos."""

    @jwt_required
    def get(self, id=None):
        """Read de um relacionamento."""
        if id == None:
            return {"status": "relacionamento invalido!"}, 400
        relacionamento = Relacionamentos.query.get(id)
        return ({"relacionamento": RelacionamentosSchema().dump(relacionamento)}, 200) \
            if relacionamento else ({"status": "relacionamento não encontrado!"}, 400)

    @jwt_required
    def post(self, id=None):
        """Create de um relacionamento."""
        try:
            if id:
                return {"status": "erro não envie o id de um relacionamento!"}, 400
            dados = RelacionamentosSchema().load(self.parser.parse_args())
            top1 = Topicos.query.get(dados['topicos1_id'])
            top2 = Topicos.query.get(dados['topicos2_id'])
            if top1 and top2:
                rel = Relacionamentos(dados['descricao'])
                rel.topicos1 = top1
                rel.topicos2 = top2
                current_app.db.session.add(rel)
                current_app.db.session.commit()
                return {
                    "status": "cadastrado!",
                    "relacionamento": RelacionamentosSchema().dump(rel)
                }, 201
            return {"status": "Topicos relacionados não existem!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    def put(self, id=None):
        """Update de um relacionamento."""
        try:
            if id == None:
                return {"status": "relacionamento invalido!"}, 400
            dados = RelacionamentosSchema().load(self.parser.parse_args())
            rel = Relacionamentos.query.get(id)
            top1 = Topicos.query.get(dados['topicos1_id'])
            top2 = Topicos.query.get(dados['topicos2_id'])
            if top1 and top2:
                rel.descricao = dados['descricao']
                rel.topicos1 = top1
                rel.topicos2 = top2
                current_app.db.session.add(rel)
                current_app.db.session.commit()
                return {
                    "status": "atualizado!",
                    "relacionamento": RelacionamentosSchema().dump(rel)
                }, 200
            return {"status": "Topicos relacionados não existem!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400

    @jwt_required
    def delete(self, id=None):
        """Delete de um relacionamento."""
        if id == None:
            return {"status": "relacionamento invalido!"}, 400
        rel = Relacionamentos.query.get(id)
        if rel:
            current_app.db.session.delete(rel)
            current_app.db.session.commit()
            return {
                "status": "deletado!"
            }
        return {"status": "erro relacionamento não encontrado!"}, 400


class RelacionamentoValidacao(Resource):
    """."""

    parser = RequestParser()
    parser.add_argument("status", type=int, choices=range(3))

    @jwt_required
    @verificaNivel
    def post(self, id=None):
        """Create de um relacionamento."""
        try:
            if id == None:
                return {"status": "relacionamento invalido!"}, 400
            rel = Relacionamentos.query.get(id)
            status_disp = {1: "Aceito!", 2: "Rejeitado!", 0: "Em Espera!"}
            if rel:
                status = self.parser.parse_args()['status']
                rel.status = status
                current_app.db.session.add(rel)
                current_app.db.session.commit()
                return {
                    "status": status_disp[status],
                    "relacionamento": RelacionamentosSchema().dump(rel)
                }, 201
            return {"status": "Relacionamento não existe!"}, 400
        except ValidationError as m_error:
            return m_error.normalized_messages(), 400


class RelacionamentoGrafo(Resource):
    """."""

    def get(self):
        """."""
        relacionamentos = Relacionamentos.query.filter(Relacionamentos.status == 1).all()
        nodes = []
        edges = []
        for i in relacionamentos:
            if not ({'id': i.topicos1.id, 'label': i.topicos1.nome} in nodes):
                nodes.append({'id': i.topicos1.id, 'label': i.topicos1.nome})
            if not ({'id': i.topicos2.id, 'label': i.topicos2.nome} in nodes):
                nodes.append({'id': i.topicos2.id, 'label': i.topicos2.nome})
            if not ({'from': i.topicos1.id, 'to': i.topicos2.id} in edges):
                edges.append({'from': i.topicos1.id, 'to': i.topicos2.id})
        return {
            "nodes": nodes,
            "edges": edges
        }
