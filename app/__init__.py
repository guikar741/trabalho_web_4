"""Modulo Principal do Projeto."""
from flask import Flask, jsonify
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_restful import Api
from .models import config_db_ma
from .resources import Login
from .resources.curso import Curso, CursoTodos
from .resources.usuario import Usuario, UsuarioTodos
from .resources.disciplina import Disciplina, DisciplinaTodas
from .resources.topico import Topico, TopicoTodos
from .resources.relacionamento import (
    Relacionamento,
    RelacionamentoTodosAceitos,
    RelacionamentoTodosDesativados,
    RelacionamentoValidacao,
    RelacionamentoGrafo
)


def start_app() -> Flask:
    """Inicia o App."""
    app = Flask(__name__)
    app.config.from_object("app.config.Config")

    CORS(app)
    config_db_ma(app)
    api = Api(app)
    JWTManager(app)

    @app.errorhandler(404)
    def page_not_found(e):  # pylint: disable=unused-variable
        """Error Page Not Found."""
        return {"status": "pagina não encontrada!"}

    @app.errorhandler(500)
    def server_error(e):  # pylint: disable=unused-variable
        """Error Internal Server Error."""
        return {"status": "erro no servidor!"}

    @app.errorhandler(401)
    def unauthorized(e):  # pylint: disable=unused-variable
        """Error unauthorized."""
        return {"status": "erro não autorizado!"}

    api.add_resource(
        Login,
        '/api/login',
        '/api/login/'
    )
    api.add_resource(
        Curso,
        '/api/cursos',
        '/api/cursos/',
        '/api/cursos/<int:id>',
        '/api/cursos/<int:id>/',
    )
    api.add_resource(
        CursoTodos,
        '/api/cursos/todos',
        '/api/cursos/todos/',
    )
    api.add_resource(
        Disciplina,
        '/api/disciplinas',
        '/api/disciplinas/',
        '/api/disciplinas/<int:id>',
        '/api/disciplinas/<int:id>/',
    )
    api.add_resource(
        DisciplinaTodas,
        '/api/disciplinas/todas',
        '/api/disciplinas/todas/',
    )
    api.add_resource(
        TopicoTodos,
        '/api/topicos/todos',
        '/api/topicos/todos/',
    )
    api.add_resource(
        Topico,
        '/api/topicos',
        '/api/topicos/',
        '/api/topicos/<int:id>',
        '/api/topicos/<int:id>/',
    )
    api.add_resource(
        RelacionamentoTodosAceitos,
        '/api/relacionamentos/aceitos',
        '/api/relacionamentos/aceitos/',
    )
    api.add_resource(
        RelacionamentoTodosDesativados,
        '/api/relacionamentos/espera',
        '/api/relacionamentos/espera/',
    )
    api.add_resource(
        Relacionamento,
        '/api/relacionamentos',
        '/api/relacionamentos/',
        '/api/relacionamentos/<int:id>',
        '/api/relacionamentos/<int:id>/',
    )
    api.add_resource(
        RelacionamentoValidacao,
        '/api/relacionamentos/validar',
        '/api/relacionamentos/validar/',
        '/api/relacionamentos/validar/<int:id>',
        '/api/relacionamentos/validar/<int:id>/',
    )
    api.add_resource(
        RelacionamentoGrafo,
        '/api/relacionamentos/grafo',
        '/api/relacionamentos/grafo/',
    )
    api.add_resource(
        UsuarioTodos,
        '/api/usuarios/todos',
        '/api/usuarios/todos/',
    )
    api.add_resource(
        Usuario,
        '/api/usuarios',
        '/api/usuarios/',
        '/api/usuarios/<int:id>',
        '/api/usuarios/<int:id>/',
    )

    return app
